#!/usr/bin/env bats

setup() {
    PUT=$BATS_TEST_DIRNAME/dh-make-ptux
    export DH_MAKE_PTUX_DATA_DIR=$BATS_TEST_DIRNAME
    TESTDIR=$(mktemp --tmpdir --directory $(basename $BATS_TEST_FILENAME).XXXXXXXX)
    test -d "$TESTDIR"
}

teardown() {
    rm -rf $TESTDIR
}

check_debian_dir() {
    debian=$1
    test -d "$debian"
    test -f "$debian/control"
    grep-dctrl --exact-match --field Package testpackage --quiet "$debian/control"
}

@test "Debian directory is created in DIR" {
    cd $TESTDIR
    $PUT testpackage
    check_debian_dir testpackage/debian
}

@test "Debian directory is created in pwd" {
    cd $TESTDIR
    mkdir testpackage
    cd testpackage
    $PUT
    check_debian_dir debian
}

@test "Debian directory is created in ." {
    cd $TESTDIR
    mkdir testpackage
    cd testpackage
    $PUT .
    check_debian_dir debian
}

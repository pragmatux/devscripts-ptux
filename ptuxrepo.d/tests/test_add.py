from . import fake_package
from . import fixture


package1 = fake_package.make(version="1.0", n_binaries=2)
package2 = fake_package.make(version="2.0", n_binaries=2)


class Add(fixture.InEmptyRepo):
    def __init__(self, *args):
        super(Add, self).__init__(*args)

    def test_deb(self):
        'adds a .deb file'

        for deb in package1.debs:
            fixture.cli('add', '.', deb)
        self.assert_has_binaries(package1.spec)

    def test_changes(self):
        'adds a .changes file'

        fixture.cli('add', '.', package1.changes)
        self.assert_has_binaries(package1.spec)

    def test_discover(self):
        'discovers and adds the current .changes file'

        with fixture.cwd(package1.source_dir):
            fixture.cli('add', self.repo.path)

        self.assert_has_binaries(package1.spec)

    def test_replaces(self):
        'add replaces old package versions'

        fixture.cli('add', '.', package1.changes)
        self.assert_has_binaries(package1.spec)

        fixture.cli('add', '.', package2.changes)
        self.assert_has_binaries(package2.spec)          
        self.assert_no_binaries(package1.spec)

    def test_force_dist(self):
        'add --dist-force DIST creates new distribution'

        dist = 'testing'
        fixture.cli('add', '--dist-force', dist, '.', package1.changes)
        self.assert_has_binaries(package1.spec, dist=dist)

    def test_reponame(self):
        'repo comes from config if name is specified'

        with fixture.tempcwd():
            c = ('repos:\n'
                 '  test/funky:\n'
                 '    path: {}\n'.format(self.repo.path))
            with file('.ptuxrepo.conf', 'w') as f:
                f.write(c)

            fixture.cli('add', 'test/funky', package1.changes)
            self.assert_has_binaries(package1.spec)

    def test_reponame(self):
        'repo comes from config default if specified'

        with fixture.tempcwd():
            c = ('default:\n'
                 '  repo: test/funky\n'
                 'repos:\n'
                 '  test/funky:\n'
                 '    path: {}\n'.format(self.repo.path))

            with fixture.cwd(package1.source_dir):
                with open('.ptuxrepo.conf', 'w') as f:
                    f.write(c)
                fixture.cli('add')

            self.assert_has_binaries(package1.spec)

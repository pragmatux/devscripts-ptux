import unittest
import tempfile
import os
import shutil
import sys
import io
from contextlib import contextmanager
import debian.deb822
import ptuxrepo.cli

def cli(*args):
    return ptuxrepo.cli.main(args)


class Repo(object):
    def __init__(self):
        self.path = tempfile.mkdtemp(prefix='ptuxrepo-')
        cli('init', self.path)

    def close(self):
        shutil.rmtree(self.path)

    def has_binaries(self, spec, dist='master'):
        is_found = {b['Package']: False for b in spec.binaries}
        packages = os.path.join(self.path, 'dists/%s/main/binary-amd64/Packages' % dist)
        with open(packages) as f:
            for p in debian.deb822.Packages.iter_paragraphs(f):
                name = p['package']
                if name in is_found and p['version'] == spec.version:
                    is_found[name] = True
                    if all(is_found.values()):
                        return True
            else:
                return False


class InEmptyRepo(unittest.TestCase):
    def setUp(self):
        self.repo = Repo()
        os.chdir(self.repo.path)

    def tearDown(self):
        self.repo.close()

    def tree(self):
        import subprocess
        subprocess.call(('tree', '-s', '.'))

    def assert_has_binaries(self, spec, dist='master'):
        self.assertTrue(self.repo.has_binaries(spec, dist))

    def assert_no_binaries(self, spec, dist='master'):
        self.assertFalse(self.repo.has_binaries(spec, dist))


@contextmanager
def stdout(command, *args, **kwargs):
    save = sys.stdout
    sys.stdout = io.StringIO()
    try:
        command(*args, **kwargs)
        sys.stdout.seek(0)
        yield sys.stdout.read()
    finally:
        sys.stdout = save


@contextmanager
def cwd(newdir):
    olddir = os.getcwd()
    try:
        os.chdir(newdir)
        yield
    finally:
        os.chdir(olddir)


@contextmanager
def tempdir():
    try:
        d = tempfile.mkdtemp(prefix='ptuxrepo-')
        yield d
    finally:
        shutil.rmtree(d)


@contextmanager
def tempcwd():
    with tempdir() as d:
        with cwd(d):
            yield

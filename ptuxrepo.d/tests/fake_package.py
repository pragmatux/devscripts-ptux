import atexit
from datetime import datetime
import os
import tempfile
import textwrap
import shutil
import subprocess


MODULE = 'fake_package'


class Spec:
    def __init__(self, name="dummy", version="1.0", n_binaries=1):
        self.version = version
        self.source = {
            'Source': f'{name}',
            'Section': 'devel',
            'Priority': 'optional',
            'Maintainer': 'Satoshi Nakamoto <satoshin@pragmatux.org>',
            'Build-Depends': 'debhelper-compat (= 13)',
            'Standards-Version': '4.6.2',
        }

        self.binaries = []
        for i in range(0, n_binaries):
            self.binaries.append({
                'Package': f'{name}' if i == 0 else f'{name}-extra{i}',
                'Architecture': 'all',
                'Depends': '${misc:Depends}',
                'Description': f'package {name} v{version}\n Enjoy!',
            })


class Context:
    def __init__(self, prefix=f'{MODULE}-'):
        self._directory = tempfile.mktemp(prefix=prefix)
        atexit.register(self.release)

    def mkdir(self, relpath):
        abspath = os.path.join(self._directory, relpath)
        os.makedirs(abspath)
        return abspath

    def release(self):
        if self._directory is not None:
            atexit.unregister(self.release)
            shutil.rmtree(self._directory)
            self._directory = None

    def __enter__(self):
        return self

    def __exit__(self, kind, val, tb):
        self.release()


class Source:
    '''A manifestation of a package Spec that has a filesystem representation
    which can be passed to other tools.
    '''
    def __init__(self, context, spec):
        self.spec = spec
        name = spec.source['Source']
        self.directory = context.mkdir(f'{name}-{spec.version}')

    def open(self, path, mode=0o644):
        '''Return an open file within the source package at the given path.'''
        full_path = os.path.join(self.directory, path)
        directory = os.path.dirname(full_path)
        os.makedirs(directory, exist_ok=True)
        return open(full_path, 'w', opener=lambda path, flags: os.open(path, flags, mode))


def make_source(context, spec):
        package = Source(context, spec)

        def dump(out, map):
            for key, value in map.items():
                out.write(f'{key}: {value}\n')

        with package.open('debian/control') as control:
            dump(control, spec.source)
            for b in spec.binaries:
                control.write('\n')
                dump(control, b)

        with package.open('debian/changelog') as changelog:
            timestamp = datetime.now().strftime('%a, %d %b %Y %H:%M:%S')
            changelog.write(textwrap.dedent(f'''\
                {spec.source['Source']} ({spec.version}) unstable; urgency=low

                  * This is only a test.

                 -- {spec.source['Maintainer']}  {timestamp} +0000

                '''))

        with package.open('debian/rules', mode=0o755) as rules:
            rules.write(textwrap.dedent(f'''\
                #!/usr/bin/make -f
                %: 
                \tdh $@
                '''))

        with package.open('debian/copyright') as copyright:
            copyright.write('''\
                Copyright: 2024, pragmatux.org
                License: Proprietary
                ''')

        with package.open('debian/source/format') as format:
            format.write('3.0 (native)')

        return package


class Build:
    def __init__(self, source):
        self.source = source

    @property
    def spec(self):
        return self.source.spec

    @property
    def source_dir(self):
        return self.source.directory

    @property
    def build_dir(self):
        return os.path.join(self.source.directory, '..')

    @property
    def deb(self):
        spec = self.spec
        if len(spec.binaries) == 1:
            return self.debs[0]
        else:
            raise ValueError('build has multiple binaries, use .debs[0]')

    @property
    def debs(self):
        version = self.spec.version
        binaries = self.spec.binaries
        return tuple(os.path.join(
            self.build_dir, f'{b["Package"]}_{version}_all.deb') for b in binaries)

    @property
    def changes(self):
        name = self.spec.source['Source']
        return os.path.join(self.build_dir, f'{name}_{self.spec.version}_amd64.changes')


def make_build(source):
    result = subprocess.run(('debuild', '--no-sign'), cwd=source.directory,
                            stdout=subprocess.DEVNULL)
    result.check_returncode()
    return Build(source)


def make(**kwargs):
    spec = Spec(**kwargs)
    context = Context()
    source = make_source(context, spec)
    return make_build(source)

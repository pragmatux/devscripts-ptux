#!/usr/bin/env python3
"""usage: ptuxconf [--file=<file>] <key> <default>

Read configuration key-value pairs from a common configuration file for
Pragmatux devscript commands. Configuration files are in TOML.

The configuration file is located by searching the following paths in order.
The *last* file found is used. Cascading may be implemented in the future.

    debian/ptux.toml
"""


FILES = ("debian/ptux.toml",)


import tomllib


def read(files=FILES):
    conf = {}

    if isinstance(files, str):
        files = (files,)
    for file in files:
        try:
            with open(file, "rb") as toml:
                conf = tomllib.load(toml)
        except FileNotFoundError:
            if files == FILES:
                # don't complain about defaults
                pass
            else:
                raise

    return conf


_cached = None


def get(key, default=None):
    global _cached
    if _cached is None:
        _cached = read()

    return _cached.get(key, default)


if __name__ == "__main__":
    import docopt
    args = docopt.docopt(__doc__)
    key = args["<key>"]
    default = args["<default>"]
    file = args["--file"]

    if file:
        conf = read(file)
        print(conf.get(key, default))
    else:
        print(get(key, default))

#!/usr/bin/env python2
"""Unit test for ptuxversion.

This is a unit test for ptuxversion using the python -m unittest protocol. To
run, execute this file directly or use one of the many unittest discover
methods. See the documentation for the unittest module.
"""

import os
import ptuxutil.sh.contrib
import ptuxversion
import re
import shutil
import tempfile
import unittest

from parameterized import parameterized


class TemporaryGitRepo:
    """A git repository and working copy created in a temporary directory.

       The creator is responsible for calling close() after use to release the
       temporary directory.
    """
    def __init__(self, clone_from=None, shallow=False):
        git = ptuxutil.sh.contrib.git
        self.dir = tempfile.mkdtemp(prefix=__name__)

        if clone_from is None:
            git.init(self.dir)
        else:
            args = ['file://{}'.format(clone_from.dir), self.dir]
            if shallow:
                args.insert(0, '--depth=1')
            git.clone(args)

        self.git = git.bake(C=self.dir)

    def close(self):
        shutil.rmtree(self.dir)

    def commit(self, msg):
        """Create an empty commit at HEAD. Returns it's commitish."""
        self.git.commit('--allow-empty', m=msg)
        abbrev = self.git('rev-parse', '--short', 'HEAD').strip()
        return abbrev

    def branch(self, name):
        """Create branch <name> at HEAD."""
        self.git.branch(name)

    def checkout(self, name):
        """Checkout branch <name> at HEAD."""
        self.git.checkout(name)

    def tag(self, name):
        """Tag with <name> at HEAD."""
        self.git.tag(name)


class WithRepo(unittest.TestCase):
    def setUp(self):
        self.repo = TemporaryGitRepo()

    def tearDown(self):
        self.repo.close()

    def verify(self, committish, expected):
        """TestCase.assert* ptuxversion generates the expected version string.

        Args: 
            commitish:
                The git commitish---e.g., sha, tag or branch name--to verify.

            expected:
                A pattern of the expected version string returned by
                ptuxversion for <commitish>. The pattern is matched verbatim,
                with the substrings `<abbrev>` and `<epoch>` substituted
                appropriately.
        """ 
        expected = re.escape(expected)
        abbrev = self.repo.git('rev-parse', '--short', committish).strip()
        expected = expected.replace('\\<abbrev\\>', abbrev)
        expected = expected.replace('\\<epoch\\>', '[0-9]{10}')

        got = ptuxversion.describe(committish, directory=self.repo.dir)
        debug = 'git log at failure\n{}'.format(
                self.repo.git.log('--oneline', '--graph', '--all', '--decorate'))
        self.assertRegexpMatches(got, '^{}$'.format(expected), msg=debug)

    def commit(self, expected):
        """Create an empty commit at HEAD and verify the expected ptuxversion."""
        c = self.repo.commit(expected)
        self.verify(c, expected)
        return c

    def tag(self, tag, verify=True):
        """Create a tag at HEAD and optionally verify the expected ptuxversion."""
        self.repo.tag(tag)
        if verify:
            vendor, sep, basetag = tag.rpartition('/')
            expected = '{}~g<abbrev>'.format(basetag)
            self.verify(tag, expected)

    def test_golden(self):
        """Test a golden commit history.

        Test a declaration of a commit tree in chronological order, with the
        ptuxversion each commit is expected to have. This is a good
        illustration and test of how ptuxversion works. The ptuxversion of each
        commit and tag is verified as it is added to the tree.
        """
        commit = self.commit
        tag = self.tag
        branchpoint = self.repo.branch
        checkout = self.repo.checkout

        # along the publishing branch
        commit('1~g<abbrev>')
        commit('2~g<abbrev>')
        branchpoint('nonpub')
        commit('3~g<abbrev>')
        tag('ptux/10')
        commit('10+1~g<abbrev>')
        commit('10+2~g<abbrev>')
        branchpoint('aftertag')

        # along a non-publishing branch
        checkout('nonpub')
        commit('2+T<epoch>~g<abbrev>')
        commit('2+T<epoch>~g<abbrev>')
        tag('ptux/10.1')
        commit('10.1+T<epoch>~g<abbrev>')
        commit('10.1+T<epoch>~g<abbrev>')

        # along a non-publishing branch after a tag on the publishing branch
        checkout('aftertag')
        commit('10+2+T<epoch>~g<abbrev>')

    def test_dirty(self):
        """Test a dirty working copy."""
        self.commit('1~g<abbrev>')
        filename = os.path.join(self.repo.dir, 'dummy')
        open(filename, 'a').close()
        self.verify('HEAD', '1+T<epoch>~g<abbrev>~dirty')
        self.tag('ptux/42', verify=False)
        self.verify('HEAD', '42+T<epoch>~g<abbrev>~dirty')

    def test_unmangle(self):
        """Tags are unmangled per DEP14."""
        tagname = 'ptux/2%3_beta.#.1.#'
        unmangled = '2:3~beta..1.'
        self.commit('1~g<abbrev>')
        self.tag(tagname, verify=False)
        self.verify('HEAD', unmangled + '~g<abbrev>')

    @parameterized.expand((
        'master',
        'origin/master',
        'main',
        'origin/main',
    ))
    def test_publishing_branch(self, name):
        self.commit('1~g<abbrev>')
        self.repo.git.branch('--move', 'master', 'tmp')
        self.verify('HEAD', '0+T<epoch>~g<abbrev>')
        self.repo.git.branch('--move', 'tmp', name)
        self.verify('HEAD', '1~g<abbrev>')


class TestShallow(unittest.TestCase):
    def setUp(self):
        self.regular = TemporaryGitRepo()
        self.regular.commit('1')
        self.regular.commit('2')
        self.shallow = TemporaryGitRepo(clone_from=self.regular, shallow=True)

    def tearDown(self):
        self.shallow.close()
        self.regular.close()

    def test_raise_on_shallow(self):
        with self.assertRaises(ValueError):
            ptuxversion.describe('HEAD', directory=self.shallow.dir)


if __name__ == '__main__':
    unittest.main()

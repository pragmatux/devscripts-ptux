#!/usr/bin/python3
"""
usage: ptuxversion [options] [<commit-ish>]

options:
  --check-development   return 0 if not released, otherwise 1
  -v --verbose          log verbose rationale to stderr

<commit-ish> defaults to HEAD if omitted.

ptuxversion is like git-describe(1), but outputs Debian package version
strings. The version strings are inferred from the location of the commit in
the commit graph according to the following rules:

1.  ptuxversion finds the most recently published ancestor of the commit.
    The most recently published ancestor is the git-merge-base(1) with the
    first of the following branches found to exist, the *publishing* branch:

    - <publishing_branch from debian/ptux.toml>@{upstream}
    - <publishing_branch from debian/ptux.toml>
    - origin/<publishing_branch from debian/ptux.toml>
    OR
    - master@{upstream}
    - master
    - origin/master
    - main@{upstream}
    - main
    - origin/main

    The strategy is to use explicit settings if available, otherwise fall back
    to reasonable defaults. Try @{upstream} first, because an upstream on a
    branch is an explicit setting. If the local branch itself doesn't exist,
    try the remote `origin`, because the default behavior in GitLab CI/CD is to
    create no local branches and name the remote from which the code is fetched
    `origin`.

2.  An initial field of the version string is derived from this most recently
    published ancestor. In the simplest case, the version string is the
    number of commits to the beginning of history: i.e., the inclusive count of
    commits found walking the commit graph from the ancestor to the beginning
    of history (using git-rev-list(1) --count).

    If a tag of the form `ptux/<base>` is found while walking, the walk is
    truncated and the count represents the distance to the nearest tag. In this
    case, the initial field of the version string is the tag `<base>` plus the
    distance: e.g., if a tag `ptux/16` exists, a commit three commits later may
    have the version `16+3`.

3.  If the commit is not along the publishing branch (see #1 above), it is
    considered a development version, and a timestamp field is added: e.g.,
    `16+3+T1682109749`. Using a timestamp means the latest build along a
    development branch is considered the newest for upgrades. This supports
    rebasing, etc.

    The timestamp begins with a capital T so it orders before fields starting
    with a branch name. It uses seconds past the epoch to conserve the limited
    version string length.

4.  For unambiguous identification, an abbreviated git object name starting
    with `~g` is always appended: e.g., `16+3+T1682109749~g5c3e798`.

5.  When the commit is HEAD, if the working tree has a local modification,
    `~dirty` is appended as a final field: `16+3+T1682109749~g5c3e798~dirty`. 

The end result a version string that mostly Just Works(TM) in development and
CI/CD without tedious, manual changelog additions or tags; has unambiguous
traceability between package and commit; and can be overridden by tags in
special circumstances.

Unusual commit graph topographies may produced unexpected results. 

Version numbers are mangled and unmangled according to DEP14 to cope with
restrictions on the format of git tags.

ptuxversion returns an error if run in a shallow git repository with no
`ptux/<base>` tags reachable from the most recently published ancestor.
"""


import docopt
import logging
import os
import ptuxconf
import ptuxutil.sh
import ptuxutil.sh.contrib
import string
import sys
import time

default_branch="master"
log = logging.getLogger('ptuxversion')

class _Repo:
    """Wrap a git repository, adding knowledge of its location and adding
    higher-level operations. Should not encapsulate ptuxversion policy."""
    def __init__(self, directory):
        self.directory = directory
        self.git = ptuxutil.sh.contrib.git.bake(C=directory)

        if self.is_shallow():
            # None of this works on shallow repositories without suitable tags.
            # For now, error on any shallow repository. Accomodating tags is
            # future work.
            raise ValueError('shallow repositores are not supported')

    def is_shallow(self):
        git_dir = self.git('rev-parse', '--git-dir').stdout.decode().strip()
        flag = os.path.join(self.directory, git_dir, 'shallow')
        return os.path.exists(flag)

    def distance(self, commit, base=None):
        if base:
            arg = '{}..{}'.format(base, commit)
        else:
            arg = commit

        count = self.git('rev-list', '--count', arg)
        return int(count)

    def are_synonomous(self, c1, c2):
        diff = self.git('rev-list', '{}...{}'.format(c1, c2))
        return not bool(diff)

    def is_dirty(self):
        modified = self.git('ls-files', modified=True, others=True, exclude_standard=True)
        return bool(modified)

    def is_contained(self, commit, by):
        if not by: return False
        excess = self.git('rev-list', commit, '^{}'.format(by))
        return not bool(excess)

    def nearest_tag(self, commit, patterns, limit=None):
        try:
            tag = self.git.describe(
                    '--tags',
                    '--abbrev=0',
                    ['--match={}'.format(p) for p in patterns],
                    commit).strip()
        except ptuxutil.sh.ErrorReturnCode:
            return None

        if limit:
            if not self.is_contained(limit, by=tag):
                return None

        return tag

    def tag_plus_distance(self, commit, patterns, base=None):
        tag = self.nearest_tag(commit, patterns, base)
        
        if tag is None:
            # If there is no tag, return the distance from the base
            distance_from_base = self.distance(commit, base)
            return str(distance_from_base)
        else:
            tag_stripped = _strip_vendor(_unmangle_tag(tag))
            distance_from_tag = self.distance(commit, tag)
            
            RENUMBER_PREFIX = '='
            if tag_stripped.startswith(RENUMBER_PREFIX):
                try:
                    # Integer-convertable tags prefixed with RENUMBER_PREFIX
                    # indicate a resumption of commit-count numbering, starting
                    # at the integer value in the tag.
                    tag_integer = int(tag_stripped[len(RENUMBER_PREFIX):])
                    return str(tag_integer + distance_from_tag)
                except ValueError:
                    pass

            if distance_from_tag != 0:
                # For normal tags, return the tag plus the distance from the tag,
                # if any.
                return f'{tag_stripped}+{distance_from_tag}'
            else:
                return tag_stripped

    def is_valid(self, commit):
        try:
            sha = self.git('rev-parse', '--verify', '{}^{{commit}}'.format(commit))
        except ptuxutil.sh.ErrorReturnCode_128:
            return False

        return True


def _branches():
    # Generate publishing branch names to search. Use values from configuration
    # file or default to master then main. Check for an upstream first.
    branch = ptuxconf.get('publishing_branch', None)
    branches = (branch,) if branch is not None else ('master', 'main')
    for b in branches:
        yield f'{b}@{{upstream}}'
        yield f'{b}'
        yield f'origin/{b}'


def _nearest_published(repo, commit):
    for b in _branches():
        if repo.is_valid(b):
            base = repo.git('merge-base', commit, b).strip()
            log.info(f'using publishing branch: {b}')
            break
    else:
        base = None

    patterns = ('ptuxversion/*', 'ptux/*')
    tag = repo.nearest_tag(commit, patterns, limit=base)

    if tag is not None:
        return tag
    else:
        return base


def is_development(repo, commit, directory=os.getcwd()):
    return (repo.are_synonomous(commit, 'HEAD') and repo.is_dirty()) \
           or not repo.is_contained(commit, _nearest_published(repo, commit))


def _strip_vendor(tag):
    vendor, sep, basetag = tag.rpartition('/')
    return basetag


def _unmangle_tag(tag):
    'Unmangle a tag name according to DEP14'
    tab = {ord('%'): ord(':'), ord('_'): ord('~'), ord('#'): None}
    return tag.translate(tab)


def describe(commit='HEAD', directory=os.getcwd()):
    repo = _Repo(directory)

    n = _nearest_published(repo, commit)
    log.info(f'nearest published: {n}')

    patterns = ('ptuxversion/*', 'ptux/*')
    d = repo.tag_plus_distance(n, patterns) if n else '0'

    y = ''
    if repo.are_synonomous(commit, 'HEAD') and repo.is_dirty():
        y = '+dirty'

    t = ''
    if y or not repo.is_contained(commit, n):
        sec_since_epoch = int(time.time())
        t = '+T{}'.format(sec_since_epoch)

    r = repo.git('rev-parse', '--short', commit).strip()

    return '{}{}~g{}{}'.format(d, t, r, y)


def cli(argv=sys.argv[1:]):
    args = docopt.docopt(__doc__, argv=argv)
    if args['--verbose']:
        logging.basicConfig(level=logging.INFO)
        logging.getLogger('ptuxutil.sh').setLevel(logging.WARNING)

    commit = args['<commit-ish>'] or 'HEAD'
    directory = os.getcwd()
    repo = _Repo(directory)

    if args['--check-development']:
        if is_development(repo, commit):
            sys.exit(0)
        else:
            sys.exit(1)
    else:
        print(describe(commit, directory))


if __name__ == "__main__":
    cli()
